#!/bin/bash


mvn clean install &> /dev/null

TAG=$(basename target/*.jar .jar | cut -d - -f 2-3)
DOCKER_NAME="adnan998/devops-practices"

docker build --build-arg TAG=$TAG --build-arg JAVA_OPTION=h2 -t $DOCKER_NAME:$TAG .

docker run --name assignment -p 8090:8090 $DOCKER_NAME:$TAG
