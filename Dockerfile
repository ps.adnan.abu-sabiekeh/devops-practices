FROM openjdk:8-alpine

ARG JAVA_OPTION
ARG TAG

ENV SPRING_ENV=$JAVA_OPTION
ENV VERSION=$TAG

RUN apk update && \
    mkdir -p /opt/source-code

COPY ./target/assignment-$VERSION.jar /opt/source-code

ENTRYPOINT java -jar -Dspring.profiles.active=$SPRING_ENV /opt/source-code/assignment-$VERSION.jar
